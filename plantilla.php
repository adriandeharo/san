<!DOCTYPE html>
<html land="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="./css/bootstrap.min.css" rel="stylesheet" />  
    <script src="./js/bootstrap.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="./css/style.css"/>
    <script src="./js/main.js"></script> 
    <script src="./js/npm.js"></script> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <title>Proyecto BBDD de SAN</title>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    
</head>

<body>
<?php
        
        $nif=$_GET['nif'];
        $puerto=ucwords($_GET['puerto']);  #Lo pasa a mayusculas la primera
        include './lib/funciones.php';

        $db = conectaDb();
?>

 <!-- CABECERA -->
        
<nav class="navbar fixed-top navbar-dark bg-primary navbar-expand-lg navbar-template">
        <a class="navbar-brand" target="_blank" href="https://www.valenciaport.com/"><img src="./img/images/logo-valenciaport-home.svg" /></a>
        <h1>Caracteristicas del Balizamiento con NIF: <?php echo " ".$nif ?> </h1>
        <div class="d-flex flex-row order-2 order-lg-3">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse order-3 order-lg-2" id="navbarNavDropdown">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class="nav-link" href="./index.html" class="btn btn-success btn-sm" >CAMBIAR DE PUERTO </a></li>
                <li class="nav-item"><a class="nav-link" href="./san.php?puerto=<?php echo $puerto ?>" class="btn btn-success btn-sm" >ATRAS </a></li>
            </ul>
        </div>
</nav>  
       
        <!-- id="cuerpo" --> 
   <div class="container"> 
                            <!-- PRIMER DIV LA FOTO -->
    <div class="row" style="margin-top:100px">
        <div  class="col-sm-4"> <!-- id="divfoto" -->
                    <div id="miCarrusel" class="carousel slide" data-ride="carousel">
                          <!-- Indicators -->
                          <ul class="carousel-indicators">
                           
                           <?php
                                $contador=0;
                                $directorio = opendir("./img/$nif"); //ruta de las fotos de la baliza
                                while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
                                {
                                    if ($archivo != "." && $archivo != "..")//verificamos si hay archivo
                                    {
                                        if($contador==0)
                                            echo "<li data-target='#miCarrusel' data-slide-to='$contador' class='active'></li>";
                                        else
                                            echo "<li data-target='#miCarrusel' data-slide-to='$contador' ></li>";
                                        $contador++;
                                    }
                                    else 
                                    {
                                          //de ser un directorio lo envolvemos entre corchetes
                                    }
                                    
                                }
                                ?>
                           
                           

                          </ul>
                          <div class="carousel-inner">
                            <?php
                              $contador=0;
                              $hayfoto=false;
                                $directorio = opendir("./img/$nif"); //ruta de las fotos de la baliza
                                while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
                                {
                                    if ($archivo != "." && $archivo != "..")//verificamos si hay archivo
                                    {
                                        if($contador==0){
                                            echo "<div class='carousel-item active'> <img src=\"./img/$nif/".$archivo."\"> ";
                                            echo "<div class='carousel-caption'>". $archivo." </div> </div>";
                                            $hayfoto=true;
                                        }
                                        else{
                                            echo "<div class='carousel-item'> <img src=\"./img/$nif/".$archivo."\">";
                                            echo "<div class='carousel-caption'>". $archivo." </div> </div>";}
                                        $contador++;

                                    }
                                
                                }
                                if(!$hayfoto)
                                    echo "FOTO N/A";
                                ?>

                          </div>

                          <!-- Left and right controls -->
                          <a class="carousel-control-prev" href="#miCarrusel" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                          </a>
                          <a class="carousel-control-next" href="#miCarrusel" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                          </a>
                    </div>
                <form action="actualizarBBDDfoto.php" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="nif" value="<?php echo  $nif; ?>">
                    <input type="file" name="foto" >
                    <input type="submit" value="Subir foto" class="btn btn-primary btn-xs btn-block" />
                </form>
                    
                    
        </div>


                                <!-- SEGUNDO DIVS LA CARACTERISTICAS -->
        <div class="col-sm-4">  <!-- id="divcaracteristicas" -->
            <h1> Caracteristicas </h1>
                        <table class="table table-hover"> 
                            <?php
                                        $consultabalizamiento = "SELECT * FROM balizamiento  where nif=$nif";
                                        $consultaobservaciones = "SELECT * FROM observaciones  where nif=$nif";

                                        $balizamiento = $db->query($consultabalizamiento);
                                        $observaciones = $db->query($consultaobservaciones);


                                        if (!$balizamiento || !$observaciones) {
                                            echo "<p>Error en la consulta.</p>\n";
                                        } else {

                                            foreach ($balizamiento as $i) {
                                                $tipo=$i[tipo];
                                                echo "<tr> <th>NIF</th>     <td class='table-warning'>$i[nif]</td> </tr>";
                                                echo "<tr> <th>Num.Internacional</th>     <td class='table-warning'>$i[num_internacional]</td> </tr>";
                                                echo "<tr> <th>tipo</th>     <td class='table-warning'>$i[tipo]</td> </tr>";
                                                echo "<tr> <th>Apariencia</th>     <td class='table-warning'>$i[apariencia]</td> </tr>";
                                                echo "<tr> <th>Periodo</th>     <td class='table-warning'>$i[periodo]</td> </tr>";
                                                echo "<tr> <th>Caracteristica</th>     <td class='table-warning'>$i[caracteristica]</td> </tr>";
                                                echo "</table>";
                                                echo "<a class=\"btn btn-primary btn-xs btn-block\" href=\"actualizarCaracteristicas.php?nif=".$i[nif].
                                                "&num_internacional=".$i[num_internacional].
                                                "&tipo=".$i[tipo].
                                                "&apariencia=".$i[apariencia].
                                                "&periodo=".$i[periodo].
                                                "&caracteristica=".$i[caracteristica].         
                                                " class=\"btn btn-primary btn-lg btn-block\"\">ACTUALIZAR CARACTERISTICAS</a>";
                                             }
                                    }
                            ?>
        </div>
  
                            <!-- TERCER DIV LA LOCALIZACION -->
        <div class="col-sm-4">      <!-- id="divlocalizacion" -->
            <h1> Localizacion </h1>
                    <table class="table table-hover"> 
                        <?php
                            $consultalocalizacion = "SELECT * FROM localizacion  where nif=$nif";
                            $localizacion = $db->query($consultalocalizacion);
                            if (!$localizacion) {
                                echo "<p>Error en la consulta.</p>\n";
                            } else {
                                foreach ($localizacion as $i) {
                                    echo "<tr> <th>NIF</th>     <td class='table-warning'>$i[nif]</td> </tr>";
                                    echo "<tr> <th>Puerto</th>     <td class='table-warning'>$i[puerto]</td> </tr>";
                                    echo "<tr> <th>Numero Local</th>     <td class='table-warning'>$i[num_local]</td> </tr>";
                                    echo "<tr> <th>localizacion</th>     <td class='table-warning'>$i[localizacion]</td> </tr>";
                                    echo "<tr> <th>latitud</th>     <td class='table-warning'>$i[latitud]</td> </tr>";
                                    echo "<tr> <th>longitud</th>     <td class='table-warning'>$i[longitud]</td> </tr>";
                                    echo "</table>";
                                    echo "<a class=\"btn btn-primary btn-xs btn-block\" href=\"actualizarLocalizacion.php?nif=".$i[nif].
                                    "&puerto=".$i[puerto].
                                    "&num_local=".$i[num_local].
                                    "&localizacion=".$i[localizacion].
                                    "&latitud=".$i[latitud].
                                    "&longitud=".$i[longitud].
                                    "&tipo=".$tipo.
                                    " class=\"btn btn-primary btn-lg btn-block\"\">ACTUALIZAR LOCALIZACION </a>";
                                    }
                                }
                        ?>
        </div>
    </div>
    
    
    
    
    
     <div class="row" style="margin-top:60px">
                            <!-- CUARTO DIV EL OBSERVACIONES -->
        <div class="col-sm-10"> 
                <h1> Observaciones </h1>
            
                <table class="table"> 
                        <tr>
                            <th>Observaciones</th>
                            <th>Borrar</th>
                        </tr>
                        <?php
                                $consultaobservaciones = "SELECT * FROM observaciones  where nif=$nif";
                                $observaciones = $db->query($consultaobservaciones);
                                if (!$observaciones) {
                                    echo "<p>Error en la consulta.</p>\n";
                                } else {
                                    foreach ($observaciones as $i) {
                                        echo "<tr class='table-warning'> <td>$i[observaciones]</td> <td> <a href=\"./borrarObservacion.php?nif=$i[nif]&observaciones=$i[observaciones]\" class=\"btn btn-primary btn-xs btn-block\"> borrar </a> </td>  </tr>\n";
                                }
                            }
                        #$db= NULL;
                        ?>
                        <form action="./modBBDDobservacion.php" method="post">
                            <input type="hidden" name="nif" value="<?php echo  $nif; ?>">
                        <tr class='table-warning'> <td><input type="text" name="observacion" /></td>  <td>  <input type="submit" value="añadir" class="btn btn-primary btn-xs btn-block"/></td></tr>
                        </form>
                </table> 
                    <!-- <a href="./actualizarMantenimiento.php?nif= <?php echo $nif ?> "  class="boton1">AÑADIR MANTENIMIENTO </a> -->
        </div>
    </div>
    
    
    
    
    <div class="row" style="margin-top:60px">
                            <!-- QUINTO DIV EL MANTENIMIENTO -->
        <div class="col-sm-10"> <!-- id="divmantenimiento" -->
                <h1> Mantenimiento </h1>
            
                <table class="table"> 
                        <tr>
                            <th>Fecha</th>
                            <th>Descripcion</th>
                            <th>Borrar</th>
                        </tr>
                        <?php
                                $consultamantenimiento = "SELECT * FROM mantenimiento  where nif=$nif order by fecha DESC";
                                $mantenimiento = $db->query($consultamantenimiento);
                                if (!$mantenimiento) {
                                    echo "<p>Error en la consulta.</p>\n";
                                } else {
                                    foreach ($mantenimiento as $i) {
                                        echo "<tr class='table-warning'> <td>$i[fecha]</td> <td>$i[mantenimiento]</td> <td> <a href=\"./borrarMantenimiento.php?nif=$i[nif]&fecha=$i[fecha]&mantenimiento=$i[mantenimiento]\" class=\"btn btn-primary btn-xs btn-block\"> borrar </a> </td>  </tr>\n";
                                }
                            }
                        #$db= NULL;
                        ?>
                        <form action="./modBBDDmantenimiento.php" method="post">
                            <input type="hidden" name="nif" value="<?php echo  $nif; ?>">
                        <tr class='table-warning'><td><input type="date" name="fecha" /></td> <td><input type="text" name="mantenimiento" /></td>  <td>  <input type="submit" value="añadir" class="btn btn-primary btn-xs btn-block"/></td></tr>
                        </form>
                </table> 
                    <!-- <a href="./actualizarMantenimiento.php?nif= <?php echo $nif ?> "  class="boton1">AÑADIR MANTENIMIENTO </a> -->
        </div>
    </div>
        <br>
        
<!--        
        <div class="row">
                <div id="divmenubotones">
                        <form action="./borrarBalizamiento.php" method="post">
                            <input type="hidden" name="nif" value="<?php echo $nif; ?>">
                            <input type="submit" value="Eliminar" class="btn btn-primary btn-lg btn-block"/>
                        </form>
                </div>
        </div>-->

</div>

    <nav class="navbar navbar-light bg-primary footer" style="margin-top:60px">
                <div class="col-md-6">
					<p>Autoridad Portuaria de Valencia. Adrian de Haro © 2018 · Todos los derechos reservados</p>
                </div>
    </nav>
</body>
</html>