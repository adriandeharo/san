<!DOCTYPE html>
<html>
<head>
    <meta charset="uft-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="./css/bootstrap.min.css" rel="stylesheet" />  
    <link rel="stylesheet" type="text/css" href="./css/style.css"/>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="./js/bootstrap.min.js"></script>
    <script src="./js/main.js" type='text/javascript'></script>
    <title>Proyecto BBDD de SAN</title>
</head>

<body>
<?php
        function conectaDb()
            {

            try {
                    $db = new PDO("mysql:host=localhost;dbname=san", "root", "administrador");
                    $db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
                    return($db);
                } catch (PDOExcepton $e) {
                    //cabecera("Error grave");
                    print "<p>Error: No puede conectarse con la base de datos.</p>\n";
                    //
                    print "<p>Error: " . $e->getMessage() . "</p>\n";
                    //pie();
                    exit();
                }
            }

            $db = conectaDb();
?>
 
<!-- CABECERA -->
         <h1>Bienvenido a la BBDD de SAN. </h1>
<nav class="navbar navbar-inverse">
	<div class="container-fluid">

		<div class="navbar-header">
			<button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#barraResolGrandes">
				<span class="icon-bar"> </span>
				<span class="icon-bar"> </span>
				<span class="icon-bar"> </span>
			</button>
		</div>



		<div class="collapse navbar-collapse" id="barraResolGrandes">
			<ul class="nav navbar-nav">
				<li> <a href="#"> boton1 </a> </li>
				<li> <a href="#"> boton2 </a> </li>
				<li> <a href="#"> boton3 </a> </li>
				<li> <a href="#"> boton4 </a> </li>
			</ul>
		</div>
	</div>
</nav>
<!--        <div data-spy="affix">
            <nav class="navbar navbar-dark  bg-primary">
                <div class="container-fluid">
                    <ul class="nav navbar-nav">
                        <li><div class="itemMenu"><a target="_blank" href="https://www.valenciaport.com/"><img src="./img/images/logo-valenciaport-home.svg" /></a></div></li>     
                        <li><div class="itemMenu"><h1>Listado Balizamiento de <?php echo " ".$puerto ?></h1></div></li>
                        <li><div class="itemMenu"><a href="./index.html" class="btn btn-success btn-sm btn-lg" >CAMBIAR DE PUERTO </a></div></li>
                        <li><div class="itemMenu"><a href="./san.php" class="btn btn-success btn-sm btn-lg">ATRAS</a></div>    </li>
                    </ul>
                </div>
            </nav>
         </div> --> 

<div class="container" style="margin-top: 70px;">
    <form action="./altaNueva.php" method="post" enctype="multipart/form-data">
        <table>
            <tr> <th>NIF</th> <td><input type="text" name="nif"/></td> </tr>
            <tr> <th>Num.Internacional</th> <td><input type="text" name="num_internacional"/></td> </tr>
            <tr> <th>tipo</th> <td><select size="1" name="tipo"><option value="boya">Boya</option><option value="baliza">Baliza</option><option value="faro">Faro</option></select></td></tr>
            <tr> <th>Alcance</th> <td><input type="text" name="alcance"/></td> </tr>
            <tr> <th>Apariencia</th> <td><input type="text" name="apariencia"/></td> </tr>
            <tr> <th>Periodo</th> <td><input type="text" name="periodo"/></td> </tr>
            <tr> <th>Caracteristica</th> <td><input type="text" name="caracteristica"/></td> </tr>
        </table>
        <br>
        <table>
            <tr> <th>Puerto</th>     <td><input type="text" name="puerto"/></td> </tr>
            <tr> <th>Numero Local</th>     <td><input type="text" name="num_local"/></td> </tr>
            <tr> <th>Localizacion</th>     <td><input type="text" name="localizacion"/></td> </tr>
            <tr> <th>Latitud</th>     <td><input type="text" name="latitud"/></td> </tr>
            <tr> <th>Longitud</th>     <td><input type="text" name="longitud"/></td> </tr>
        </table>
        <br>
        <table>
            <tr> <th>Insertar foto...</th>     <td><input name="foto" type="file" /></td> </tr>
        </table>
        <input type="submit" value="ENVIAR ALTA" class="btn btn-success btn-sm"/>
    </form>


</div>
</body>
</html>